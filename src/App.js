import './App.css';
import WebcamComponent from './Component/WebcamComponent';

function App() {
  return (
    <div className="App">
      <WebcamComponent />
    </div>
  );
}

export default App;
