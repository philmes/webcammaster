import React , { useState, useRef, useCallback } from 'react';
import Webcam from 'react-webcam';
import '../CSS/WebcamComponent.css';

//npm install react-webcam

const WebcamComponent = () => {
    const webcamRef = useRef(null);
    const [imgSrc, setImgSrc] = useState(null);
    const [width, setWidth] = useState(480);
    const [height, setHeight] = useState(340);
    const [widthImg, setWidthImg] = useState(480);
    const [heightImg, setHeightImg] = useState(340);
    const [ratio, setRatio] = useState(1.7777777778);
  
    const capture = useCallback(() => {
      const imageSrc = webcamRef.current.getScreenshot();
      setImgSrc(imageSrc);
    }, [webcamRef, setImgSrc]);

    const close = () => {
      setImgSrc(null);
    };

    const handleCapSize = (event) => {
      event.preventDefault();
      setWidth(event.target.largura.value);
      setHeight(event.target.altura.value);
      setWidthImg(event.target.larguraImg.value);
      setHeightImg(event.target.alturaImg.value);
      setRatio(event.target.ratio.value);
    }

    const videoConstraints = {
      width: {width},
      height: {height},
      facingMode: "user",
      aspectRatio: { ideal: `${ ratio }` }
    };

  return(
    <div className='webcams' style={{ marginTop: '10px'}}>
      <Webcam 
        audio={false}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        width={widthImg}
        height={heightImg}
        videoConstraints={videoConstraints}
      />
      <div>
        <form onSubmit={handleCapSize}>
          <label>Definicao</label>
          <input type="text" name="largura" placeholder="Largura" alt="Largura" defaultValue={width} />
          <input type="text" name="altura" placeholder="Altura" alt="Altura" defaultValue={height} />
          <br></br>
          <label>Quadro Size</label>
          <input type="text" name="larguraImg" placeholder="Largura da Img" alt="Largura da Img" defaultValue={widthImg} />
          <input type="text" name="alturaImg" placeholder="Altura da Img" alt="Altura da Img" defaultValue={heightImg} />
          <label>Quadro Size</label><br></br>
          <input type="text" name="ratio" placeholder="Ratio" alt="Ratio" defaultValue={ratio} />
          <button type="submit">Alterar Definicoes</button>
        </form>
        <button onClick={capture}>Capture photo</button>
        <button onClick={close}>Close photo</button>
      </div>
      <div>
        {imgSrc && (
          <img
            src={imgSrc}
            alt=""
          />
        )}
      </div>
    </div>
  )
};
export default WebcamComponent;